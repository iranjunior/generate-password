const express = require("express");
const faker = require("faker");
const app = express();

app.use(express.json());

app.get("/", (request, response) => {
  response.sendFile(`${__dirname}/index.html`);
});
app.post("/generate", (request, response) => {
  const { serial } = request.body;
  const object = {
    serial,
    password: faker.internet.password()
  };

  return response
    .status(201)
    .json(object)
    .send();
});
app.listen(3000, () => {
  console.log("Start service");
});
